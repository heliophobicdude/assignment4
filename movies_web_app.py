import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode
import pprint
pp = pprint.PrettyPrinter(indent=4)

from werkzeug.datastructures import ImmutableMultiDict


application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()

    table_ddl = ("CREATE TABLE movies(id int(10) UNSIGNED NOT NULL AUTO_INCREMENT, year text NOT NULL, title text NOT NULL, director text NOT NULL, actor text NOT NULL, release_date text NOT NULL, rating DOUBLE PRECISION(10,2) NOT NULL, PRIMARY KEY(id))")
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
        populate_data()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


def populate_data():

    db, username, password, hostname = get_db_creds()

    print("Inside populate_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                       host=hostname,
                                       database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("INSERT INTO message (greeting) values ('Hello, World!')")
    cnx.commit()
    print("Returning from populate_data")


def query_data():

    db, username, password, hostname = get_db_creds()

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    cur.execute("SELECT greeting FROM message")
    entries = [dict(greeting=row[0]) for row in cur.fetchall()]
    return entries

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table global")
    create_table()
    print("After create_data global")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None


@app.route('/add_movie', methods=['POST'])
def add_movie():
    print("Received request.")
    print(request.form)
    msg = request.form

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    try:
        cur.execute("INSERT INTO movies (year, title, director, actor, release_date, rating) values (%(year)s, %(title)s, %(director)s, %(actor)s, %(release_date)s, %(rating)s)", msg)
        cnx.commit()
    except:
        pass
    cur.close()
    cnx.close()

    return hello("Movie {title} successfully inserted".format(title=msg["title"]))

@app.route('/update_movie', methods=['POST'])
def update_movie():
    form = request.form
    title = form.get('title')
    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    cur = cnx.cursor(buffered=True)
    try:
        q = "SELECT title FROM movies WHERE title={0!r:}".format(title)
        print(q)
        cur.execute(q)
        cnx.commit()
    except mysql.connector.Error as err: 
        pp.pprint(err.msg)
        # cur.close()
        # cnx.close()

    if cur.rowcount < 1:
        msg = "Movie {} not updated - {}.".format(title, "movies not found")
        return render_template('index.html', message=msg)

    try:
        q = 'UPDATE movies SET ' + ', '.join("{!s} = {!r}".format(k, v) for k, v in form.items() if k != 'title') + ' WHERE title={0!r:}'.format(title)
        print(q)
        cur.execute(q)
        cnx.commit()
    except mysql.connector.Error as err: 
        pp.pprint(err.msg)
        # cur.close()
        # cnx.close()

    cur.close()
    cnx.close()
    msg = "Movie {} successfully updated".format(title)
    return render_template("index.html", message=msg)

@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    form = request.form
    title = form.get('title')
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    try:
        cur.execute("SELECT title FROM movies WHERE title='{0!s:}'".format(title))
    except Exception as e:
        raise e
    
    check = len(cur.fetchall()) > 0
    if not check:
        msg = "Movie {} hasn't been deleted. - {}".format(title, "movie doesn't exist.")
        return render_template('index.html', message=msg)


    try:
        q = "DELETE FROM movies WHERE title='{0!s:}'".format(title)
        cur.execute(q)
        cnx.commit()
    except mysql.connector.Error as err: 
        pp.pprint(err.msg)
        cur.close()
        cnx.close()

    cur.close()
    cnx.close()
    msg = "The movie {} successfully deleted.".format(title)
    return render_template('index.html', message=msg)

@app.route('/search_movie', methods=['GET'])
def search_movie():
    args = request.args
    actor = args.get('actor')
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor(buffered=True)
    try:
        q = "SELECT * FROM movies WHERE actor='{0!s:}'".format(actor)
        print(actor)
        print(q)
        cur.execute(q)
    except Exception as e:
        raise e

    if cur.rowcount < 1:
        msg = "Movie with actor {} not found. - {}".format(actor, "actor doesn't exist.")
        return render_template('index.html', message=msg)
    
    actors = [list(entry) for entry in cur.fetchall()]
    results = []

    for actor in actors:
        result = "{title}, {year}, {actor}\n".format(title=actor[2], year=actor[1], actor=actor[4])
        results.append(result)

    cur.close()
    cnx.close()
    msg = "Movies with this actor have been found. Here is the list:"
    return render_template('index.html', message=msg, movies=results)


@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor(buffered=True)
    try:
        q = "SELECT * FROM movies WHERE rating=(SELECT MIN(rating) FROM movies)"
        print(q)
        cur.execute(q)
    except Exception as e:
        raise e

    if cur.rowcount < 1:
        msg = "Movie with lowest rating not found."
        return render_template('index.html', message=msg)
    
    movies = [list(entry) for entry in cur.fetchall()]
    results = []

    """<title, year, actor, director, release_date, rating> one per line """
    for movie in movies:
        result = "{title}, {year}, {actor}, {director}, {rating}\n".format(title=movie[2], year=movie[1], actor=movie[3], director=movie[4], rating=movie[6])
        results.append(result)

    cur.close()
    cnx.close()
    msg = "Movies with this actor have been found. Here is the list:"
    return render_template('index.html', message=msg, movies=results)

@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor(buffered=True)
    try:
        q = "SELECT * FROM movies WHERE rating=(SELECT MAX(rating) FROM movies)"
        print(q)
        cur.execute(q)
    except Exception as e:
        raise e

    if cur.rowcount < 1:
        msg = "Movie with lowest rating not found."
        return render_template('index.html', message=msg)
    
    movies = [list(entry) for entry in cur.fetchall()]
    results = []

    """<title, year, actor, director, release_date, rating> one per line """
    for movie in movies:
        result = "{title}, {year}, {actor}, {director}, {rating}\n".format(title=movie[2], year=movie[1], actor=movie[3], director=movie[4], rating=movie[6])
        results.append(result)

    cur.close()
    cnx.close()
    msg = "Movies with this actor have been found. Here is the list:"
    return render_template('index.html', message=msg, movies=results)

@app.route("/")
def hello(info="Welcome to Alex IMDB"):
    print("Inside hello")
    print("Printing available environment variables")
    # print(os.environ)
    print("Before displaying index.html")
    msg = info
    return render_template('index.html', message=msg)


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
